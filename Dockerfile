FROM openjdk:8-jre-alpine
VOLUME /tmp
COPY target/*.jar ms-product.jar
ENTRYPOINT ["java","-jar","ms-product.jar"]
